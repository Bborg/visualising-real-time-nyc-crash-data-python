#testCollisionData

from pykafka import KafkaClient

from urllib.request import urlopen
import json
import time
import pandas as pd 
from sodapy import Socrata
from datetime import datetime

url = 'https://data.cityofnewyork.us/resource/bpv4-gfc4.json'


#Kafka Producer
client = KafkaClient(hosts="localhost:9092")

topic =client.topics['CollisionData']

producer = topic.get_sync_producer()




# JSON object imported from url 
json_obj = urlopen(url)

data=json.load(json_obj)

#for loop goes through each dictionary in the list, manipulating the crash_date variable to include both the date and time.
for i in range(len(data)):
    x = data[i]["crash_date"]
    x=x[:10]

    if(len(data[i]["crash_time"]) == 4):
        z=data[i]["crash_time"]
        data[i]["crash_time"] = "0"+ z

    y=x + "_" + data[i]["crash_time"]
    data[i]["crash_date"] = y

    data[i]["longitude"]=float(data[i]["longitude"])
    data[i]["latitude"]=float(data[i]["latitude"])
    

    

#sorting in ascending order by crash_date
sortedData=sorted(data, key=lambda date: date["crash_date"],reverse=True)

#Adding the markerIndex element to the dictionary. (to be used later)
for i in range(len(sortedData)):
    sortedData[i]["markerIndex"]= i%50
    





#Function to loop through data producing each dict in list of collisions to kafka
def produce_Collisions(data):
    i=0
    while i < len(data):
        message = json.dumps(sortedData[i])
        print (message)
        producer.produce(message.encode('ascii'))
        time.sleep(1)
        i += 1


produce_Collisions(data)






